nnoremap <leader>r :call RestartApp()<cr>
function! RestartApp()
  let p = getcwd()
  let cmd = " C-c ' cd " . p . " ; docker-compose up' Enter"
  call TmuxSendKey("main", "mongoapp", cmd)
endfunction
