#!/usr/bin/env ruby

require 'rack'
require_relative "main"

run Rack::URLMap.new(
  '/' => MongoApp.new,
)
