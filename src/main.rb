require 'sinatra'
require 'mongo'
require 'pry-remote'

class MongoApp < Sinatra::Base
  configure do
    set :erb, trim: '-'
  end

  def db
    $mongodb_client ||= Mongo::Client.new(
      ENV['MONGO_PATH'] || 'mongodb://dbuser:dbpass@127.0.0.1:27017/appdb', max_pool_size: 25
    )
  end

  def get_topic
    db[:topic].find.first
  end

  post '/topic/?' do
    db[:topic].drop
    db[:topic].insert_one(
      question: params[:topic][:question],
      yes_count: 0,
      no_count: 0,
    )
    redirect '/'
  end

  post '/vote/:answer/?' do |answer|
    topic = get_topic
    db[:topic].update_one({"_id" => topic["_id"]}, {'$set' => {
      "#{answer}_count" => topic["#{answer}_count"] + 1
    }})
    redirect '/'
  end

  post '/vote/no/?' do
    redirect '/'
  end

  get '/topic/?' do
    erb :topic
  end

  get '/?' do
    sleep rand(0.5..1.0)
    erb :index, locals: {topic: get_topic}
  end
end
